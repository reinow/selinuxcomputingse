---
title: "Certifiering"
date: 2020-04-08T12:14:34+02:00
description: "Certifiering - Common Criteria"
---

## Certifiering - Common Criteria

Inom den amerikanska administrationen har sedan länge säkerhetsklassificerad information krävt säkerhetscertifierade datorsystem. Säkerhetspolicyn implementeras i operativsystemet, och den färdiga konfigurationen certifieras enligt Common Criteria.

Datorsystem baserade på SELinux har certifierats enligt Common Criteria Evaluation and Validation Scheme (CCEVS) och erhållit nivå EAL4+ (Evaluation Assurance Level 4 Augmented) gentemot skyddsprofilerna LSPP, CAPP och RBACPP. Det är sannolikt den högsta möjliga nivån för ett datorsystem med ett operativsystem som är användbart för vitt skilda ändamål.

* LSPP: Labeled Security Protection Proﬁle
* CAPP: Controlled Access Protection Proﬁle
* RBACPP: Role Based Access Control Protection Proﬁle

