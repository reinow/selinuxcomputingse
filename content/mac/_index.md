---
title: "MAC"
date: 2020-03-27T12:14:34+02:00
description: "MAC - Mandatory Access Control"
---

## Mandatory Access Control
Inom IT-säkerhet refererar *MAC (Mandatory Access Control)*, påtvingad åtkomstkontroll, till en typ av åtkomstkontroll där operativsystemet begränsar möjligheten för ett subjekt att utföra en operation på ett objekt. Ett subjekt är vanligen en process eller en tråd. Bland objekt kan nämnas filer, bibliotek, TCP/UDP-portar, segment av delat minne, men också andra processer än subjektet själv.

<div class="container-fluid">
        <div class="row">
            <div class="col-xs-12 col-md-6 text-center">
                <figure>
                    <img class="img-responsive" src="/images/dac.png" alt="DAC - Discretionary Access Control" title="DAC - Discretionary Access Control" />
                    <figcaption>Diskret åtkomstkontroll</figcaption>
                </figure>
            </div>
            <div class="col-xs-12 col-md-6 text-center">
                <figure>
                    <img class="img-responsive" src="/images/mac.png" alt="MAC - Mandatory Access Control" title="MAC - Mandatory Access Control" />
                    <figcaption>Påtvingad åtkomstkontroll</figcaption>
                </figure>          
            </div>
</div>

Varje subjekt och varje objekt är försedda med ett antal säkerhetsattribut. Då ett subjekt försöker utföra en operation på ett objekt utvärderar operativsystemet utan undantag om denna operation är förenlig med det omfattande och detaljerade regelverk som utgör säkerhetspolicyn.

## Vad kan MAC åstadkomma?

* Stark separation av processer
* Stark separation av data
  * Separerar data baserat på konfidentialitet/integritet/ändamål
* System-, applikation- och datasäkerhet
  * Skydd mot icke auktoriserad modifiering
  * Skydd mot avsiktlig icke önskvärd modifiering
* Begränsning av programs privilegier
  * Exekvera kod på ett säkert vis även om koden inte verifierats som säker
  * Säkerställa att programfel inte kan leda till ökade privilegier
  * Begränsa varje programs rättigheter enligt principen om minsta möjliga privilegier
* Garanti för säker processering genom hela kedjan
  * Säkerställa att data processeras på så sätt som är föreskrivet
  * Dela upp processeringen i minimala säkra steg
* Begränsningar vad gäller auktorisering
  * Dela upp administratörsrollen
  * Dela in användare i klasser baserat på position och clearance

