---
title: "SELinux"
date: 2020-03-31T12:14:34+02:00
description: "SELinux är ett flexibelt ramverk för påtvingad åtkomstkontroll, Mandatory Access Control (MAC), för Linux."
---

## Security Enhanced Linux

SELinux är ett flexibelt ramverk för påtvingad åtkomstkontroll, [Mandatory Access Control (MAC)](/mac/), för Linux som är integrerat i linuxkärnan via Linux Security Modules API. SELinux grundar sig på många års forskning om säkra operativsystem inom NSA, National Security Agency.

SELinux - Security Enhanced Linux - utvecklas av NSA, säkerhetskonsulter och oberoende programmerare från hela världen. SELinux gör det närmast omöjligt för obehöriga användare eller program att på ett otillåtet sätt ta kontroll över processer, filer eller hårdvara, och därmed viktig information.

SELinux begränsar möjligheten för ett subjekt att utföra en operation på ett objekt. Ett subjekt är en process, och bland objekten kan nämnas filer, bibliotek, TCP/UDP-portar, segment av delat minne, men också andra processer än subjektet själv. Varje subjekt och varje objekt är försedda med ett antal säkerhetsattribut. Då ett subjekt försöker utföra en operation på ett objekt utvärderar operativsystemet utan undantag om denna operation är förenlig med det omfattande och detaljerade regelverk som utgör säkerhetspolicyn.

<img class="img-fluid rounded mx-auto d-block" style="margin-bottom: 40px;" src="https://www.selinuxcomputing.se/images/stories/selinux_flow.jpg" alt="SELinux">

## Unik flexibilitet
SELinux är speciellt i bemärkelsen att det finns en strikt separation mellan beslut (decision) och verkställande av besluten (enforcement). Med detta förfarande erhålles en flexibilitet som unik, och möjliggör att säkerhetspolicyn hanteras dynamiskt. Man kan därför använda samma operativsystem för vitt skilda ändamål, vilket möjliggör att kostnaden kan reduceras dramatiskt i jämförelsen med traditionella MAC-system.

## Säkerhetsmodeller
Ett godtyckligt antal säkerhetsmodeller kan kombineras inom ramen för SELinux, och deras sammantagna verkan kan analyseras inom ett gemensamt policyschema. För närvarande innehåller SELinux följande säkerhetsmodeller:

* Type Enforcement (TE)
* Role Based Access Control (RBAC)
* Identity Based Access Control (IBAC)
* Multi Level Security (MLS)
* Multi Category Security (MCS)

Alla säkerhetsmoduler är ortogonala mot Linux Discretionary Access Control (DAC) schema.

## Vi tillhandahåller

* Utveckling och implementering av säkerhetspolicies för SELinux
* Programvara som tjänst på Internet, tillhandahållna med våra SELinux baserade servrar
* Drift av SELinux-servrar åt kunder


