# Setup

The Oribium theme for [Hugo](https://gohugo.io) is a fork of Themefisher's [kross hugo](https://github.com/themefisher/kross-hugo) theme.

## Licensing

- Copyright 2019 Themefisher (https://themefisher.com/)
- Licensed under MIT

